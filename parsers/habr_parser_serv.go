package parsers

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
	"goParser/models"
	"net/http"
	strconv "strconv"
	"strings"
)

const maxTries = 5

type HabrParserServ struct {
	wd    selenium.WebDriver
	query string
}

func GetParser(query, silenium string) (*HabrParserServ, error) {
	if len(query) <= 0 {
		query = "golang"
	}
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}
	chrCaps := chrome.Capabilities{
		Args: []string{"--disable-dev-shm-usage"},
		W3C:  true,
	}
	caps.AddChrome(chrCaps)

	var urlPrefix = selenium.DefaultURLPrefix
	if len(silenium) > 0 {
		urlPrefix = fmt.Sprintf("http://%s/wd/hub", silenium)
	}

	var wd selenium.WebDriver
	var err error
	i := 1
	for i < maxTries {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			i++
			continue
		}
		break
	}
	return &HabrParserServ{wd: wd, query: query}, err
}

func (parser *HabrParserServ) Quit() error {
	if parser.wd != nil {
		return parser.wd.Quit()
	}
	return nil
}

func (parser *HabrParserServ) GetTotalVacancies() (int, error) {

	err := parser.wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?q=%s&type=all", parser.query))
	if err != nil {
		return 0, err
	}

	elements, err := parser.wd.FindElements(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return 0, err
	}

	totalVacanciesTtext, err := elements[0].Text()
	if err != nil {
		return 0, err
	}

	totalVacancies, err := strconv.ParseInt(strings.Split(totalVacanciesTtext, " ")[1], 0, 32)
	if err != nil {
		return int(totalVacancies), err
	}

	return int(totalVacancies), nil
}

func (parser *HabrParserServ) GetLinksToVacancies(pages int) ([]string, error) {
	var links []string
	page := 1
	for i := 0; i < pages; page++ {

		err := parser.wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, parser.query))
		if err != nil {
			return nil, err
		}

		elements, err := parser.wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			return nil, err
		}

		for j := range elements {
			href, err := elements[j].GetAttribute("href")
			if err != nil {
				continue
			}
			links = append(links, fmt.Sprintf("https://career.habr.com%s", href))
		}
		i += len(elements)
	}
	return links, nil
}

func GetHabrVacancy(vacancy string) (*models.HabrVacancy, int, error) {
	resp, err := http.Get(vacancy)
	if err != nil {
		return nil, 0, err
	}

	document, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, 0, err
	}

	dd := document.Find("script[type=\"application/ld+json\"]")
	if err != nil {
		return nil, 0, fmt.Errorf("habr vacancy %s nodes not found", vacancy)
	}

	var habrVacancy models.HabrVacancy

	habrVacancySTR := dd.First().Text()
	err = json.Unmarshal([]byte(habrVacancySTR), &habrVacancy)
	if err != nil {
		return nil, 0, err
	}

	id, err := strconv.ParseInt(strings.Split(vacancy, "/")[4], 0, 64)
	if err != nil {
		return nil, 0, err
	}

	return &habrVacancy, int(id), nil

}

func (parser *HabrParserServ) Parse() (map[int]models.HabrVacancy, error) {

	totalVacancies, err := parser.GetTotalVacancies()
	if err != nil {
		return nil, err
	}

	vacancies, err := parser.GetLinksToVacancies(totalVacancies)
	if err != nil {
		return nil, err
	}

	habrVacancies := make(map[int]models.HabrVacancy, len(vacancies))
	for _, vacancy := range vacancies {
		habrVacancy, id, err := GetHabrVacancy(vacancy)
		if err != nil {
			continue
		}
		habrVacancies[id] = *habrVacancy
	}
	return habrVacancies, nil
}

func (parser *HabrParserServ) FiendQuery(query string) (map[int]models.HabrVacancy, error) {
	parser.query = query
	return parser.Parse()
}
