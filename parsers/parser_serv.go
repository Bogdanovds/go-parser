package parsers

import "goParser/models"

type ParserServ interface {
	Quit() error
	GetTotalVacancies() (int, error)
	Parse() (map[int]models.HabrVacancy, error)
	FiendQuery(query string) (map[int]models.HabrVacancy, error)
}
