package main

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	httpHandler "goParser/handler"
	"goParser/parsers"
	"goParser/repository"
	"goParser/services"
	"net/http"
	"os"
	"os/signal"
	"time"
)

var (
	parserServ          parsers.ParserServ
	vacanciesRepository repository.VacanciesRepository
	vacanciesService    services.VacanciesService
	handler             httpHandler.Handler
)

func getVars() *mongo.Client {
	var err error
	silenium := os.Getenv("SILENIUM_VAR")
	parserServ, err = parsers.GetParser("", silenium)
	if err != nil {
		panic(err)
	}

	var client *mongo.Client
	vacanciesRepository, client, err = repository.GetMongoHabrVacanciesRepo(parserServ, nil, "")
	if err != nil {
		panic(err)
	}

	vacanciesService = &services.HabrVacanciesServ{Repo: vacanciesRepository}

	handler = httpHandler.Handler{VacanciesService: vacanciesService}
	return client
}

func main() {
	client := getVars()

	srv := &http.Server{Addr: ":8080", Handler: handler.Routing()}

	go func() {
		defer client.Disconnect(context.TODO())
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			panic(err)
		}

	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancelCnt := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelCnt()
	err := srv.Shutdown(ctx)
	if err != nil {
		panic(err)
	}
}
