package handler

import (
	"github.com/go-chi/chi/v5"
	"net/http"
)

func (h *Handler) vacanciesRout(r chi.Router) {
	r.Post("/search/{query}", func(w http.ResponseWriter, r *http.Request) {
		query := chi.URLParam(r, "query")
		err := h.VacanciesService.Search(query)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		WriteOK(w)
	})

	r.Delete("/delete/{id}", func(w http.ResponseWriter, r *http.Request) {
		idS := chi.URLParam(r, "id")
		err := h.VacanciesService.Delete(idS)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		WriteOK(w)
	})

	r.Get("/get/{id}", func(w http.ResponseWriter, r *http.Request) {
		idS := chi.URLParam(r, "id")

		vacancy, err := h.VacanciesService.Get(idS)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, err = w.Write(vacancy)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}
	})

	r.Get("/list", func(w http.ResponseWriter, r *http.Request) {
		vacancies, err := h.VacanciesService.List()
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, err = w.Write(vacancies)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}
	})

}
