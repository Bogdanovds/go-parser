package handler

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"goParser/docs"
	"goParser/services"
	"net/http"
)

type Handler struct {
	VacanciesService services.VacanciesService
}

func (h *Handler) Routing() *chi.Mux {
	r := chi.NewRouter()

	r.Group(h.vacanciesRout)

	r.Get("/swagger", docs.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./module4/webserver/swagger/homework/public"))).ServeHTTP(w, r)
	})

	return r
}

func WriteErr(err error, w http.ResponseWriter, statusCode int) bool {
	if err != nil {
		if statusCode == 0 {
			statusCode = http.StatusInternalServerError
		}
		w.WriteHeader(statusCode)
		marshal, marshalErr := json.Marshal(err)
		if marshalErr != nil {
			_, _ = w.Write([]byte(err.Error()))
		}
		_, _ = w.Write(marshal)
	}
	return err != nil
}

func WriteOK(w http.ResponseWriter) {
	vacanciJSON, _ := json.Marshal("OK")

	_, _ = w.Write(vacanciJSON)
}
