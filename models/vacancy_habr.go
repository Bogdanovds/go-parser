package models

type HabrVacancy struct {
	Context            string     `json:"@context" bson:"context"`
	Type               string     `json:"@type" bson:"type"`
	DatePosted         string     `json:"datePosted" bson:"datePosted"`
	Title              string     `json:"title" bson:"title"`
	Description        string     `json:"description" bson:"description"`
	Identifier         Identifier `json:"identifier" bson:"identifier"`
	ValidThrough       string     `json:"validThrough" bson:"validThrough"`
	HiringOrganization struct{}   `json:"hiringOrganization" bson:"hiringOrganization"`
	EmploymentType     string     `json:"employmentType" bson:"employmentType"`
	JobLocationType    string     `json:"jobLocationType,omitempty" bson:"jobLocationType,omitempty"`
	//JobLocation        []struct{} `json:"jobLocation"`
}

type HiringOrganization struct {
	Type   string `json:"@type"`
	Name   string `json:"name"`
	Logo   string `json:"logo"`
	SameAs string `json:"sameAs"`
}

type Identifier struct {
	Type  string `json:"@type" bson:"type"`
	Name  string `json:"name" bson:"name"`
	Value string `json:"value" bson:"value"`
}

type JobLocationElement struct {
	Type    string `json:"@type"`
	Address string `json:"address"`
}

type PurpleJobLocation struct {
	Type    string  `json:"@type"`
	Address Address `json:"address"`
}

type Address struct {
	Type            string         `json:"@type"`
	StreetAddress   string         `json:"streetAddress"`
	AddressLocality string         `json:"addressLocality"`
	AddressCountry  AddressCountry `json:"addressCountry"`
}

type AddressCountry struct {
	Type string `json:"@type"`
	Name string `json:"name"`
}

type JobLocationUnion struct {
	JobLocationElementArray []JobLocationElement
	PurpleJobLocation       *PurpleJobLocation
}
