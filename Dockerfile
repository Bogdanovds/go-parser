FROM golang:1.19-bullseye

ARG SILENIUM
ENV SILENIUM_VAR=$SILENIUM

WORKDIR /app
COPY . .

RUN go mod tidy -v
RUN go build -o main main.go

EXPOSE 30001
CMD ["/app/main", "$SILENIUM_VAR"]