package docs

import "goParser/models"

// swagger:route POST /search/{query} vacancies searchVacancies
// Fiend vacancies for query
//
// responses:
//  200: description: "OK"
//  404: description: Internal server error

// swagger:parameters searchVacancies
//
//nolint:all
type searchVacancies struct {
	// query of search vacancies
	// required: true
	// in:query
	Query string `json:"query "`
}

// swagger:route DELETE /delete/{id} vacancies deleteVacancy
// Delete vacancies for id
//
// responses:
//  200: description: "OK"
//  404: description: Internal server error

// swagger:parameters deleteVacancy
//
//nolint:all
type deleteVacancy struct {
	// id of vacancy
	// required: true
	// in:query
	ID int `json:"id "`
}

// swagger:route GET /get/{id} vacancies getVacancy
// Get vacancy for id
//
// responses:
//  200: getVacancyResponses
//  404: description: Internal server error

// swagger:parameters getVacancy
//
//nolint:all
type getVacancy struct {
	// id of vacancy
	// required: true
	// in:query
	ID int `json:"id "`
}

// swagger:response getVacancyResponses
//
//nolint:all
type getVacancyResponses struct {
	// in:body
	Body models.HabrVacancy
}

// swagger:route GET /list vacancies getVacanciesList
// Get all vacancies
//
// responses:
//  200: getVacanciesListResponses
//  404: description: Internal server error

// swagger:response getVacanciesListResponses
//
//nolint:all
type getVacanciesListResponses struct {
	// in:body
	Body []models.HabrVacancy
}
