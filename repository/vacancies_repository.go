package repository

import "goParser/models"

type VacanciesRepository interface {
	Create(dto models.HabrVacancy) error
	GetByID(id int) (models.HabrVacancy, error)
	GetList() ([]models.HabrVacancy, error)
	Delete(id int) error
	Search(query string) error
}
