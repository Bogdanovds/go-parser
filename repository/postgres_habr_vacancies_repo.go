package repository

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"goParser/models"
	"goParser/parsers"
)

type PostgresHabrVacanciesRepo struct {
	parserServ parsers.ParserServ
	db         *sqlx.DB
}

func GetPostgresHabrVacanciesRepo(parserServ parsers.ParserServ, host, port, user, password, dbname string) (PostgresHabrVacanciesRepo, error) {

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sqlx.Open("postgres", psqlInfo)
	if err != nil {
		return PostgresHabrVacanciesRepo{}, err
	}

	return PostgresHabrVacanciesRepo{parserServ: parserServ, db: db}, nil
}

func (repo PostgresHabrVacanciesRepo) Create(dto models.HabrVacancy) error {
	_, err := repo.db.Exec(
		"INSERT INTO vacancies ("+
			"context, "+
			"type, "+
			"date_posted, "+
			"title, "+
			"description, "+
			"identifier, "+
			"valid_through, "+
			"hiring_organization, "+
			"employment_type, "+
			"job_location_type) "+
			"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)",
		dto.Context,
		dto.Type,
		dto.DatePosted,
		dto.Title,
		dto.Description,
		dto.Identifier,
		dto.ValidThrough,
		dto.HiringOrganization,
		dto.EmploymentType,
		dto.JobLocationType)
	if err != nil {
		return err
	}
	return nil

}

func (repo PostgresHabrVacanciesRepo) GetByID(id int) (models.HabrVacancy, error) {
	vacancy := models.HabrVacancy{}
	err := repo.db.Get(&vacancy, "SELECT * FROM vacancies WHERE id=$1", id)
	if err != nil {
		return models.HabrVacancy{}, err
	}
	return vacancy, nil
}

func (repo PostgresHabrVacanciesRepo) GetList() ([]models.HabrVacancy, error) {
	var vacancies []models.HabrVacancy
	err := repo.db.Select(&vacancies, "SELECT * FROM vacancies")
	if err != nil {
		return nil, err
	}
	return vacancies, nil
}

func (repo PostgresHabrVacanciesRepo) Delete(id int) error {
	_, err := repo.db.Exec("DELETE FROM vacancies WHERE id=$1", id)
	if err != nil {
		return err
	}

	return nil
}

func (repo PostgresHabrVacanciesRepo) Search(query string) error {
	fiendQuery, err := repo.parserServ.FiendQuery(query)
	if err != nil {
		return err
	}

	_, err = repo.db.Exec("TRUNCATE TABLE vacancies")
	if err != nil {
		return err
	}

	tx := repo.db.MustBegin()
	for _, vacancy := range fiendQuery {
		tx.MustExec(
			"INSERT INTO vacancies ("+
				"context, "+
				"type, "+
				"date_posted, "+
				"title, "+
				"description, "+
				"identifier, "+
				"valid_through, "+
				"hiring_organization, "+
				"employment_type, "+
				"job_location_type) "+
				"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)",
			vacancy.Context,
			vacancy.Type,
			vacancy.DatePosted,
			vacancy.Title,
			vacancy.Description,
			vacancy.Identifier,
			vacancy.ValidThrough,
			vacancy.HiringOrganization,
			vacancy.EmploymentType,
			vacancy.JobLocationType)
	}
	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}
