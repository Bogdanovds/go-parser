package repository

import (
	"fmt"
	"goParser/models"
	"goParser/parsers"
	"math"
)

type RamHabrVacanciesRepo struct {
	parserServ parsers.ParserServ
	vacancy    map[int]models.HabrVacancy
}

func GetVacanciesRepo(parserServ parsers.ParserServ) (RamHabrVacanciesRepo, error) {
	parse, err := parserServ.Parse()
	if err != nil {
		return RamHabrVacanciesRepo{parserServ: parserServ}, err
	}
	return RamHabrVacanciesRepo{parserServ: parserServ, vacancy: parse}, nil
}

func (repo RamHabrVacanciesRepo) Create(dto models.HabrVacancy) error {
	for i := 0; i < math.MaxInt; i++ {
		_, ok := repo.vacancy[i]
		if !ok {
			repo.vacancy[i] = dto
			return nil
		}
	}
	return fmt.Errorf("map overflow")
}

func (repo RamHabrVacanciesRepo) GetByID(id int) (models.HabrVacancy, error) {
	return repo.vacancy[id], nil
}

func (repo RamHabrVacanciesRepo) GetList() ([]models.HabrVacancy, error) {
	v := make([]models.HabrVacancy, 0, len(repo.vacancy))

	for id, value := range repo.vacancy {
		repo.vacancy[id] = value
		v = append(v, value)
	}
	return v, nil
}

func (repo RamHabrVacanciesRepo) Delete(id int) error {
	delete(repo.vacancy, id)
	return nil
}

func (repo RamHabrVacanciesRepo) Search(query string) error {
	fiendQuery, err := repo.parserServ.FiendQuery(query)
	if err != nil {
		return err
	}
	repo.vacancy = fiendQuery
	return nil
}
