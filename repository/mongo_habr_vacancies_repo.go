package repository

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"goParser/models"
	"goParser/parsers"
)

type MongoHabrVacanciesRepo struct {
	parserServ parsers.ParserServ
	db         *mongo.Database
}
type habrVacancyWrapper struct {
	Id    int                `bson:"_id"`
	Value models.HabrVacancy `bson:"value"`
}

func GetMongoHabrVacanciesRepo(parserServ parsers.ParserServ, clientOptions *options.ClientOptions, uri string) (MongoHabrVacanciesRepo, *mongo.Client, error) {
	if clientOptions == nil {
		clientOptions = options.Client()
	}
	if len(uri) <= 0 {
		uri = "mongodb://127.0.0.1:27017"
	}

	client, err := mongo.NewClient(clientOptions.ApplyURI(uri))
	if err != nil {
		return MongoHabrVacanciesRepo{}, nil, err
	}

	err = client.Connect(context.Background())
	if err != nil {
		return MongoHabrVacanciesRepo{}, client, err
	}

	db := client.Database("habrVacancies")

	return MongoHabrVacanciesRepo{parserServ, db}, client, err
}

func (repo MongoHabrVacanciesRepo) Create(dto models.HabrVacancy) error {
	collection := repo.db.Collection("vacancies")

	_, err := collection.InsertOne(context.TODO(), habrVacancyWrapper{Value: dto})
	if err != nil {
		return err
	}

	return nil
}

func (repo MongoHabrVacanciesRepo) GetByID(id int) (models.HabrVacancy, error) {
	collection := repo.db.Collection("vacancies")
	result := collection.FindOne(context.TODO(), bson.M{"_id": id})
	if result.Err() != nil {
		return models.HabrVacancy{}, result.Err()
	}

	var habrVacancy habrVacancyWrapper
	err := result.Decode(&habrVacancy)
	if err != nil {
		return models.HabrVacancy{}, err
	}

	return habrVacancy.Value, nil
}

func (repo MongoHabrVacanciesRepo) GetList() ([]models.HabrVacancy, error) {
	collection := repo.db.Collection("vacancies")
	find, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}

	vacancies := make([]models.HabrVacancy, 0, 256)
	for find.Next(context.TODO()) {
		var habrVacancy habrVacancyWrapper
		err = find.Decode(&habrVacancy)
		if err != nil {
			continue
		}

		vacancies = append(vacancies, habrVacancy.Value)
	}

	return vacancies, nil
}

func (repo MongoHabrVacanciesRepo) Delete(id int) error {
	collection := repo.db.Collection("vacancies")
	result := collection.FindOneAndDelete(context.TODO(), bson.M{"_id": id})
	if result.Err() != nil {
		return result.Err()
	}
	return nil
}

func (repo MongoHabrVacanciesRepo) Search(query string) error {
	collection := repo.db.Collection("vacancies")

	fiendQuery, err := repo.parserServ.FiendQuery(query)
	if err != nil {
		return err
	}

	_, err = collection.DeleteMany(context.TODO(), struct{}{})
	if err != nil {
		return err
	}

	for id, vacancy := range fiendQuery {
		x, err := collection.InsertOne(context.Background(), habrVacancyWrapper{Id: id, Value: vacancy})
		fmt.Println(x)
		if err != nil {
			return err
		}
	}

	return nil
}
