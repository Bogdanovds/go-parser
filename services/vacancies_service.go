package services

type VacanciesService interface {
	Search(query string) error
	Delete(idS string) error
	Get(idS string) ([]byte, error)
	List() ([]byte, error)
}
