package services

import (
	"encoding/json"
	"goParser/repository"
	"strconv"
)

type HabrVacanciesServ struct {
	Repo repository.VacanciesRepository
}

func (serv *HabrVacanciesServ) Search(query string) error {
	err := serv.Repo.Search(query)
	if err != nil {
		return err
	}
	return nil
}

func (serv *HabrVacanciesServ) Delete(idS string) error {
	id, err := strconv.ParseInt(idS, 0, 32)
	if err != nil {
		return err
	}

	err = serv.Repo.Delete(int(id))
	if err != nil {
		return err
	}

	return nil
}

func (serv *HabrVacanciesServ) Get(idS string) ([]byte, error) {
	id, err := strconv.ParseInt(idS, 0, 32)
	if err != nil {
		return nil, err
	}

	vacancy, err := serv.Repo.GetByID(int(id))
	if err != nil {
		return nil, err
	}

	vacancyJSON, err := json.Marshal(vacancy)
	if err != nil {
		return nil, err
	}

	return vacancyJSON, err
}

func (serv *HabrVacanciesServ) List() ([]byte, error) {
	vacancies, err := serv.Repo.GetList()
	if err != nil {
		return nil, err
	}

	vacanciesJSON, err := json.Marshal(vacancies)
	if err != nil {
		return nil, err
	}

	return vacanciesJSON, err
}
